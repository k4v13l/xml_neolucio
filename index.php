<?php

//Bloco que abre o arquivo zipado com 
//xmls a serem registrados em csv.
$zip = new ZipArchive;
$res = $zip->open('zip.zip');
if ($res === TRUE) {
    $zip->extractTo('xmls/');
    $zip->close();
    echo 'woot!';
} else {
    echo 'doh!';
}


//Bloco para criação da tabela em html final
//
$date = date("Y-m-d_H-i-s");
$linha1 = "";
$f = fopen("Planilhas/".$date.".html", 'w');
$filesXML = glob('xmls/*{xml}',GLOB_BRACE);
$count = 1;
fwrite($f,"<table>");
foreach ($filesXML as $filexml){
    if (file_exists($filexml)) 
    {
        if ($count == 1){fwrite($f,"<thead>");}
        if ($count != 1){fwrite($f,"<tr>");}
        $xml = simplexml_load_file($filexml);
        createCsv($xml, $f,$count);
    }
    if ($count == 1){fwrite($f,"</thead>");}
    if ($count != 1){fwrite($f,"</tr>");}
    $count++;
}
if($linha1){ fwrite($f,"<tr>".$linha1."</tr>");}else{echo "Falha";}
fwrite($f,"</table>");
fclose($f);

function createCsv($xml,$f,$count=null)
{
    global $linha1;
    foreach ($xml->children() as $item) 
    {
        $hasChild = (count($item->children()) > 0)?true:false;
        if( ! $hasChild)
        {switch ($item->getName()){
		case "CNPJ":
		case "verEvento":
		case "descEvento":
            if ($count == 1){fwrite($f,"<th>".$item->getName()."</th>");$linha1 .= "<td>".$item."</td>";}//Insere um elemento de cab e guarda a primeira linha.
            if ($count != 1){fwrite($f,"<td>".$item."</td>");}//Incrementa as demais linhas
		break;
		
        }}
        else
        {
            if ($count != 1){createCsv($item, $f);}else{createCsv($item,$f,$count);}//Operação recursiva para chegar aos elementos que não contém outros elementos
        }
    }
}
